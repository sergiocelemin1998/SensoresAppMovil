package com.example.sensores;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class PokemonActivity extends AppCompatActivity implements SensorEventListener
{
    //este valor sirve para calibrar!
    public final static  float MOVIMIENTO_LIMITE_FRACTURA = 8.0f;
    public final static float MOVIMIENTO_LIMITE_ROMPER = 12.5f;

    private float xValue, yValue, zValue;
    private float xNewValue, yNewValue, zNewValue;

    private boolean primerMovimiento = true;
    private boolean pokemonListo;
    private boolean huevoFracturado;
    private boolean movimientoIniciado;

    private SensorManager sensorManager;
    private Sensor acelerometro;
    private Sensor lightSensor;
    private Sensor stepSensor;

    private ImageView imgHuevo;
    private TextView labHuevo, txtPrueba, txtPrueba2;

    private ConstraintLayout vista;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        imgHuevo = findViewById(R.id.imgHuevo);
        labHuevo = findViewById(R.id.labHuevo);
        txtPrueba = findViewById(R.id.txtPrueba);
        txtPrueba2 = findViewById(R.id.txtPrueba2);
        vista = findViewById(R.id.clVista);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        acelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        stepSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        pokemonListo = false;
        huevoFracturado = false;
        movimientoIniciado = false;
    }


    @Override
    public void onSensorChanged(SensorEvent event)
    {
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
        {
            actualizarParametros(event.values[0], event.values[1], event.values[2]);

            if(!movimientoIniciado && cambioAceleracion())
            {
                movimientoIniciado = true;

            }
            else if(movimientoIniciado && cambioAceleracion())
            {
                romperHuevo();

            }
            else if (movimientoIniciado && !cambioAceleracion())
            {
                movimientoIniciado = false;
            }
        }
        else if(event.sensor.getType() == Sensor.TYPE_LIGHT)
        {
            if(pokemonListo)
            {
                txtPrueba.setText(event.values[0] + "");

                if(event.values[0] < 100)
                {
                    labHuevo.setTextColor(Color.WHITE);
                    txtPrueba.setTextColor(Color.WHITE);
                    txtPrueba2.setTextColor(Color.WHITE);
                    vista.setBackgroundColor(Color.DKGRAY);
                }
                else if(event.values[0] < 400)
                {
                    labHuevo.setTextColor(Color.RED);
                    txtPrueba.setTextColor(Color.RED);
                    txtPrueba2.setTextColor(Color.RED);
                    vista.setBackgroundColor(Color.YELLOW);
                }
                else
                {
                    labHuevo.setTextColor(Color.BLACK);
                    txtPrueba.setTextColor(Color.BLACK);
                    txtPrueba2.setTextColor(Color.RED);
                    vista.setBackgroundColor(Color.WHITE);
                }
            }
        }
        else if(event.sensor.getType()== Sensor.TYPE_STEP_COUNTER)
        {
            if(pokemonListo)
            {
                txtPrueba2.setText(event.values[0] + "");

                if(event.values[0] <= 10)
                {
                    imgHuevo.setImageResource(R.drawable.lluvia);

                } else if(event.values[0] <= 21 && event.values[0] >= 11 )
                {
                    imgHuevo.setImageResource(R.drawable.nieve);

                }
                else if(event.values[0] <= 32 && event.values[0] >= 22 )
                {
                    imgHuevo.setImageResource(R.drawable.soleado);

                }
                else
                {
                    imgHuevo.setImageResource(R.drawable.normal);
                }
            }

        }
    }

    private void romperHuevo()
    {
        if (huevoFracturado)
        {
            labHuevo.setText("FELICIDADES, ES UN CASTFORM!");
            imgHuevo.setImageResource(R.drawable.normal);
            pokemonListo = true;
        }
        else
        {
            labHuevo.setText("YA CASI, AGITA MÁS FUERTE!");
            imgHuevo.setImageResource(R.drawable.broken_egg);
            huevoFracturado = true;
        }
    }

    private boolean cambioAceleracion()
    {
        //debe haber un cambio en dos ejes para considerarse un shake.
        float movX = Math.abs(xValue - xNewValue);
        float movY = Math.abs(yValue - yNewValue);
        float movZ = Math.abs(zValue - zNewValue);

        if (huevoFracturado)
        {
            return ((movX > MOVIMIENTO_LIMITE_ROMPER && movY > MOVIMIENTO_LIMITE_ROMPER)
                    || (movX > MOVIMIENTO_LIMITE_ROMPER && movZ > MOVIMIENTO_LIMITE_ROMPER)
                    || (movY > MOVIMIENTO_LIMITE_ROMPER && movZ > MOVIMIENTO_LIMITE_ROMPER));
        }
        else
        {
            return ((movX > MOVIMIENTO_LIMITE_FRACTURA && movY > MOVIMIENTO_LIMITE_FRACTURA)
                    || (movX > MOVIMIENTO_LIMITE_FRACTURA && movZ > MOVIMIENTO_LIMITE_FRACTURA)
                    || (movY > MOVIMIENTO_LIMITE_FRACTURA && movZ > MOVIMIENTO_LIMITE_FRACTURA));

        }
    }

    private void actualizarParametros(float nuevaX, float nuevaY, float nuevaZ)
    {
        if (primerMovimiento)
        {
            xValue = nuevaX;
            yValue = nuevaY;
            zValue = nuevaZ;

            primerMovimiento = false;
        }
        else
        {
            xValue = xNewValue;
            yValue = yNewValue;
            zValue = zNewValue;
        }

        xNewValue = nuevaX;
        yNewValue = nuevaY;
        zNewValue = nuevaZ;
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        sensorManager.registerListener(this, acelerometro, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, stepSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }
}
